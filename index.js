require('dotenv').config()

// Kucoin SDK
const API = require('kucoin-node-sdk');

const config = {
    baseUrl: 'https://api.kucoin.com',
    apiAuth: {
        key: process.env.KEY,
        secret: process.env.SECRET,
        passphrase: process.env.PASSPHRASE,
    },
}

/** Init Configure */
API.init(config);

/** API use */
const main = async () => {

    while (true) {

        try {
            let trade = await API.rest.Trade.Orders.postOrder({
                clientOid: process.env.CLIENTOID,
                side: 'buy',
                symbol: 'SDN-USDT',
                type: 'market'
            }, {
                funds: '1000'
            })

            console.log('Trade', trade);
        } catch (error) {
            console.log('Error', error.message)
        }

        setTimeout(() => true, 100)
    }
};

main();